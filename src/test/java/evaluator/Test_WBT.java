package evaluator;

import evaluator.controller.AppController;
import org.junit.Test;

public class Test_WBT {

    @Test
    public void test_TC01() {
        try {
            AppController appController = new AppController("NumarIntrebariInvalid.txt");
            evaluator.model.Test test = appController.createNewTest();
        } catch (Exception ex) {
            assert (ex.getMessage().equals("Nu exista suficiente intrebari pentru crearea unui test!(5)"));
        }
    }

    @Test
    public void test_TC02() {
        try {
            AppController appController = new AppController("NumarDomeniiInvalid.txt");
            evaluator.model.Test test = appController.createNewTest();
        } catch (Exception ex) {
            assert (ex.getMessage().equals("Nu exista suficiente domenii pentru crearea unui test!(5)"));
        }
    }

    @Test
    public void test_TC03() {
        try {
            AppController appController = new AppController("DateCorecte.txt");
            evaluator.model.Test test = appController.createNewTest();
            assert (test.getIntrebari().size() == 5);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void test_TC04() {
        try {
            AppController appController = new AppController("DateCorecte.txt");
            evaluator.model.Test test = appController.createNewTest();
            assert (test.getIntrebari().size()==5);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void test_TC05() {
        try {
            AppController appController = new AppController("DateCorecte.txt");
            evaluator.model.Test test = appController.createNewTest();
            assert (test.getIntrebari().size() == 5);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void test_TC06(){
        try {
            AppController appController = new AppController("DateFixe.txt");
            evaluator.model.Test test = appController.createNewTest();
            assert (test.getIntrebari().size() == 5);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}