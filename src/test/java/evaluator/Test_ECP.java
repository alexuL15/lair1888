package evaluator;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import org.junit.Test;


public class Test_ECP {

    IntrebariRepository repo = new IntrebariRepository();

    @Test
    public void TC1_ECP() throws InputValidationFailedException, DuplicateIntrebareException {

        Intrebare intrebare = new Intrebare("Cate mere are Ana?","1) Multe","2) Putine","1","Agricultura");
        String result = repo.addIntrebare(intrebare);
        assert(result=="quiz added");
    }

    @Test(expected = InputValidationFailedException.class)
    public void TC11_ECP() throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("","1) Multe","2) Putine","1","Agricultura");
        String result = repo.addIntrebare(intrebare);
    }

    @Test(expected = InputValidationFailedException.class)
    public void TC2_ECP() throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("care este ceasul?","1) Multe","2) Putine","1","Agricultura");
        String result = repo.addIntrebare(intrebare);
    }

    @Test(expected = InputValidationFailedException.class)
    public void TC12_ECP() throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("Care este ceasul?","","2) Putine","1","Agricultura");
        String result = repo.addIntrebare(intrebare);
    }

    @Test(expected = InputValidationFailedException.class)
    public void TC13_ECP() throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("Care este ceasul?","1) Multe","","1","Agricultura");
        String result = repo.addIntrebare(intrebare);
    }

    @Test(expected = InputValidationFailedException.class)
    public void TC14_ECP() throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("Care este ceasul?","1) Multe","2) Putine","","Agricultura");
        String result = repo.addIntrebare(intrebare);
    }


}

