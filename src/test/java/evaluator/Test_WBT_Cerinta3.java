package evaluator;

import evaluator.controller.AppController;
import org.junit.Test;

public class Test_WBT_Cerinta3 {

    private static String VALID_DOMAIN="Intrebare1";

    @Test
    public void test_TC01() {
        try {
            AppController appController = new AppController("FisierGol.txt");
            evaluator.model.Statistica statistica = appController.getStatistica();
        } catch (Exception ex) {
            assert (ex.getMessage().equals("Repository-ul nu contine nicio intrebare!"));
        }
    }

    @Test
    public void test_TC02() {
        try {
            AppController appController = new AppController("DateCorecte.txt");
            evaluator.model.Statistica statistica = appController.getStatistica();
            assert (statistica.getNumarIntrebari(VALID_DOMAIN).equals(1));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }



}
