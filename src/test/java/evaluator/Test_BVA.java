package evaluator;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import evaluator.util.InputValidation;
import org.junit.Test;
public class Test_BVA {

    IntrebariRepository repo = new IntrebariRepository();

    @Test
    public void TC3_BVA() throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("M?","1) Multe","2) Putine","1","Agricultura");
        String result = repo.addIntrebare(intrebare);
        assert (result == "quiz added");
    }

    @Test
    public void TC9_BVA() throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("Cate mere are Ana?","1)D","2) Putine","1","Agricultura");
        String result = repo.addIntrebare(intrebare);
        assert (result == "quiz added");
    }

    @Test
    public void TC15_BVA() throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("Cate mere are Ana?","1) Multe","2)D","1","Agricultura");
        String result = repo.addIntrebare(intrebare);
        assert (result == "quiz added");
    }

    @Test
    public void TC19_BVA() throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("Cate mere are Ana?","1) Multe","2) Putine","1","Agricultura");
        String result = repo.addIntrebare(intrebare);
        assert (result == "quiz added");
    }

    @Test
    public void TC27_BVA() throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("Cate mere are Ana?","1) Multe","2) Putine","1","D");
        String result = repo.addIntrebare(intrebare);
        assert (result == "quiz added");
    }

    @Test(expected = InputValidationFailedException.class)
    public void TC20_BVA() throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("Cate mere are Ana?","1) Multe","2) Putine","4","D");
        String result = repo.addIntrebare(intrebare);
    }


}
