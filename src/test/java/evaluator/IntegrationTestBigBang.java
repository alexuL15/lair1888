package evaluator;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

@Category(IntegrationTest.class)
public class IntegrationTestBigBang {
    IntrebariRepository repo;
    AppController appController;
    private static String VALID_DOMAIN="Intrebare1";

    @Before
    public void setData(){
        repo = new IntrebariRepository();
        appController = new AppController("DateCorecte.txt");
    }

    //BBT - TC3_BVA
    @Test
    public void test_A() throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("M?","1) Multe","2) Putine","1","Agricultura");
        String result = repo.addIntrebare(intrebare);
        assert (result == "quiz added");
    }

    //WBT - Test_TC03
    @Test
    public void test_B() {
        try {
            appController = new AppController("DateCorecte.txt");
            evaluator.model.Test test = appController.createNewTest();
            assert (test.getIntrebari().size() == 5);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //Int - test_TC02
    @Test
    public void test_C() {
        try {
            evaluator.model.Statistica statistica = appController.getStatistica();
            assert (statistica.getNumarIntrebari(VALID_DOMAIN).equals(1));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void test_PABC() throws DuplicateIntrebareException, InputValidationFailedException {
        test_A();
        test_B();
        test_C();
    }
}
