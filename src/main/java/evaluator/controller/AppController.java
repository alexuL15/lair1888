package evaluator.controller;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.model.Test;
import evaluator.repository.IntrebariRepository;
import evaluator.util.InputValidation;

import java.util.LinkedList;
import java.util.List;

public class AppController {
	
	private IntrebariRepository intrebariRepository;
	String file;
	
	public AppController(String file) {
		intrebariRepository = new IntrebariRepository();
		intrebariRepository.setIntrebari(intrebariRepository.loadIntrebariFromFile(file));
	}

	public Intrebare addNewIntrebare(String enunt, String varianta1, String varianta2, String variantaCorecta, String domeniu)
			throws DuplicateIntrebareException, InputValidationFailedException {
		InputValidation.validateEnunt(enunt);
		InputValidation.validateVarianta1(varianta1);
		InputValidation.validateVarianta2(varianta2);
		InputValidation.validateVariantaCorecta(variantaCorecta);
		InputValidation.validateDomeniu(domeniu);
		Intrebare intrebare = new Intrebare(enunt, varianta1, varianta2, variantaCorecta, domeniu);
		intrebariRepository.addIntrebare(intrebare);
		return intrebare;
	}

	public void loadIntrebariFromFile(String f){
		intrebariRepository.setIntrebari(intrebariRepository.loadIntrebariFromFile("intrebari.txt"));
	}
	
	public boolean exists(Intrebare intrebare){
		return intrebariRepository.exists(intrebare);
	}
	
	public Test createNewTest() throws NotAbleToCreateTestException{
		
		if(intrebariRepository.getIntrebari().size() < 5) {
			throw new NotAbleToCreateTestException("Nu exista suficiente intrebari pentru crearea unui test!(5)");
		}else {

			if (intrebariRepository.getNumberOfDistinctDomains() < 5) {
				throw new NotAbleToCreateTestException("Nu exista suficiente domenii pentru crearea unui test!(5)");
			}else {
				List<Intrebare> testIntrebari = new LinkedList<Intrebare>();
				List<String> domenii = new LinkedList<String>();
				Intrebare intrebare;
				Test test = new Test();

				while (testIntrebari.size() != 5) {
					intrebare = intrebariRepository.pickRandomIntrebare();

					if (!testIntrebari.contains(intrebare) && !domenii.contains(intrebare.getDomeniu())) {
						testIntrebari.add(intrebare);
						domenii.add(intrebare.getDomeniu());
					}

				}

				test.setIntrebari(testIntrebari);
				return test;
			}
		}
		
	}

	public Statistica getStatistica() throws NotAbleToCreateStatisticsException{
		
		if(intrebariRepository.getIntrebari().isEmpty())
			throw new NotAbleToCreateStatisticsException("Repository-ul nu contine nicio intrebare!");
		
		Statistica statistica = new Statistica();
		for(String domeniu : intrebariRepository.getDistinctDomains()){
			statistica.add(domeniu, intrebariRepository.getNumberOfIntrebariByDomain(domeniu));
		}
		
		return statistica;
	}

}
